## Avant le premier lancement
1) Exécuter la commande `npm install` pour installer toutes les dépendances.
2) Lancer un service MYSQL en local sur votre machine.
3) Créer une base de données appelée `linkweb`.
4) Créer un utilisateur avec l'identifiant `test-technique` et le mot de passe `test123`. 
5) Vérifier que l'utilisateur ait bien les droits sur la base.

Si besoin de modifier la configuration, allez dans `app/db.config.js`.

## Pour lancer l'API
Exécuter la commande `npm run start` ou `node server.js` à la racine du projet.\
Le serveur se lancera sur le port 8080 et est accessible à l'adresse [http://127.0.0.1:8080](http://127.0.0.1:8080).\
Le seul port d'écoute autorisé est le port local 3000 (c'est-à-dire [http://localhost:3000](http://localhost:3000)). Veillez donc à ce que le service communiquant avec l'API soit sur le bon canal (sous peine d'obtenir une erreur de CORS policy).\
Si besoin de modifier un des deux ports, aller dans `/server.js`. 

## Une fois l'API lancée
Les tables se créeront automatiquement et de fausses données seront ajoutées à chaque démarrage du serveur.\
Pour annuler ce comportement par défaut, vous pouvez supprimer les lignes 15 à 55 du fichier `server.js`.
