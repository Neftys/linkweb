const db = require("../models");
const Article = db.articles;
const Category = db.categories;
const ArticleCategory = db.article_category;

/**
 * Constant used in query parameters to make a join between the two tables.
 * @type {{include: [{through: {attributes: *[]}, as: string, model}]}}
 */
const included = {
    include: [
        {
            model: Category,
            as: "categories",
            through: {
                attributes: []
            }
        }
    ]
};

/**
 * Creates an article in DB.
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.create = async (req, res) => {
    if (!req.body.title) {
        res.status(400).send({
            message: "Title can not be empty."
        });
        return;
    }
    const categories = req.body.categories;
    if (!categories || categories.length < 1 || categories.length > 2) {
        res.status(400).send({
            message: 'Categories are not correct.'
        });
        return;
    }
    const article = {
        id: req.body.id,
        title: req.body.title,
        description: req.body.description,
        createdAt: req.body.createdAt,
        content: req.body.content
    };

    await Article.create(article)
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        });

    for (let i = 0; i < categories.length; i++) {
        const articleCategory = {
            article_id: article.id,
            category_id: categories[i]
        }
        ArticleCategory.create(articleCategory);
    }
    res.status(200).send();
}


/**
 * Get all articles in DB.
 * @param req
 * @param res
 */
exports.findAll = (req, res) => {
    Article.findAll(included)
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
}

/**
 * Get one article in DB.
 * @param req Id must be in request params.
 * @param res
 */
exports.findOne = (req, res) => {
    const id = req.params.id;
    Article.findByPk(id, included)
        .then(data => {
            if(data) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: 'Article not found.'
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
};

/**
 * Update article in DB.
 * @param req Id must be in request params.
 * @param res
 */
exports.update = (req, res) => {
    const id = req.params.id;
    Article.update(req.body, {
        where: {id: id}
    }, included)
        .then(num => {
            if(num === 1) {
                res.send({
                    message: "Article was updated successfully."
                });
            } else {
                res.send({
                    message: "Cannot update this article. Maybe was not found."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
};

/**
 * Delete article in DB.
 * @param req Id must be in request params.
 * @param res
 */
exports.delete = (req, res) => {
    const id = req.params.id;
    Article.destroy({
        where: {id: id}
    }, included)
        .then(num => {
            if(num === 1) {
                res.send({
                    message: "Article was deleted successfully."
                });
            } else {
                res.send({
                    message: "Cannot delete this article because was not found."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
};

/**
 * Delete all articles in DB.
 * @param req
 * @param res
 */
exports.deleteAll = (req, res) => {
    Article.destroy({
        where: {},
        truncate: false
    }, included)
        .then(nums => {
            res.send({message: `${nums} articles were deleted.`})
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
};
