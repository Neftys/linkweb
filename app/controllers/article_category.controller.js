const db = require("../models");
const Article = db.articles;
const Category = db.categories;
const ArticleCategory = db.article_category;

/**
 * Creates an entry in the join table.
 * @param categoryId
 * @param articleId
 */
exports.create = (categoryId, articleId) => {
    Category.findByPk(categoryId)
        .then((category) => {
            if (!category) {
                console.log("Category not found!");
                return null;
            }
            Article.findByPk(articleId)
                .then((article) => {
                    if (!article) {
                        console.log("Article not found!");
                        return null;
                    }
                    const articleCategory = {
                        article_id: articleId,
                        category_id: categoryId
                    };
                    ArticleCategory.create(articleCategory)
                        .then(data => {
                            console.log(`>> added Article id=${article.id} to Category id=${category.id}`);
                        })
                        .catch(err => {
                            console.log(err);
                        })
                    ;
                });
        })
        .catch((err) => {
            console.log(">> Error while adding Article to Category: ", err);
        })
    ;
};
