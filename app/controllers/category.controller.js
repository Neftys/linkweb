const db = require("../models");
const Category = db.categories;
const Article = db.articles;

/**
 * Constant used in query parameters to make a join between the two tables.
 * @type {{include: [{through: {attributes: *[]}, as: string, model}]}}
 */
const included = {
    include: [
        {
            model: Article,
            as: "articles",
            through: {
                attributes: [],
            }
        },
    ],
}

/**
 * Creates a category in DB.
 * @param req
 * @param res
 */
exports.create = (req, res) => {
    if(!req.body.title) {
        res.status(400).send({
            message: "Title can not be empty."
        });
        return;
    }
    const category = {
        id: req.body.id,
        title: req.body.title,
        slug: req.body.slug,
        color: req.body.color
    };
    Category.create(category)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
};

/**
 * Get all categories in DB.
 * @param req
 * @param res
 */
exports.findAll = (req, res) => {
    Category.findAll()
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
};

/**
 * Get one category in DB.
 * @param req Id must be in request params.
 * @param res
 */
exports.findOne = (req, res) => {
    const id = req.params.id;
    Category.findByPk(id, included)
        .then(data => {
            if(data) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: 'Category not found.'
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
};

/**
 * Update category in DB.
 * @param req ID must be in request params.
 * @param res
 */
exports.update = (req, res) => {
    const id = req.params.id;
    Category.update(req.body, {
        where: {id: id}
    })
        .then(num => {
            if(num === 1) {
                res.send({
                    message: "Category was updated successfully."
                });
            } else {
                res.send({
                    message: "Cannot update this category. Maybe was not found"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
};

/**
 * Delete category in DB.
 * @param req Id must be in request params.
 * @param res
 */
exports.delete = (req, res) => {
    const id = req.params.id;
    Category.destroy({
        where: {id: id}
    })
        .then(num => {
            if(num === 1) {
                res.send({
                    message: "Category was deleted successfully."
                });
            } else {
                res.send({
                    message: "Cannot delete this category because was not found."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
};

/**
 * Delete all categories
 * @param req
 * @param res
 */
exports.deleteAll = (req, res) => {
    Category.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({message: `${nums} categories were deleted.`})
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        })
    ;
};
