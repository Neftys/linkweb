/**
 * Define a join table between article and category.
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    const ArticleCategory = sequelize.define("article_categories", {
        article_id: {
            type: Sequelize.BIGINT,
            allowNull: false
        },
        category_id: {
            type: Sequelize.BIGINT,
            allowNull: false
        }
    });
    return ArticleCategory;
}
