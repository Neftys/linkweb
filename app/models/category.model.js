/**
 * Define a category object.
 * @param sequelize
 * @param Sequelize
 * @returns {*}
 */
module.exports = (sequelize, Sequelize) => {
    const Category = sequelize.define("category", {
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        slug: {
            type: Sequelize.STRING,
            allowNull: false
        },
        color: {
            type: Sequelize.STRING,
            allowNull: true,
            validate: {
                is: /^(#)[A-Fa-f0-9]{6}$/
            }
        }
    });
    return Category;
}
