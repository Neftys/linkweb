const dbConfig = require("../db/db.config");
const Sequelize = require("sequelize");

// Config DB
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,
    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.categories = require("./category.model.js")(sequelize, Sequelize);
db.articles = require("./article.model.js")(sequelize, Sequelize);
db.article_category = require("./article_category.model")(sequelize, Sequelize);

// Define association between entities
db.categories.belongsToMany(db.articles, {
    through: db.article_category,
    as: "articles",
    foreignKey: "category_id"
});

db.articles.belongsToMany(db.categories, {
    through: db.article_category,
    as: "categories",
    foreignKey: "article_id"
});

module.exports = db;
