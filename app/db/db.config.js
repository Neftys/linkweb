/**
 * Database config
 */
module.exports = {
    HOST: "127.0.0.1",
    USER: "test-technique",
    PASSWORD: "test123",
    DB: "linkweb",
    dialect: "mysql",
    pool: {
        max: 2,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};
