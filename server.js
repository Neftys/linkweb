const express = require("express");
const cors = require("cors");
const app = express();
let corsOptions = {
    origin: "http://localhost:3000",
};
app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
const db = require("./app/models");

// Sync DB and populate (only for test)
db.sequelize.sync({force: true})
    .then(() => {
        db.articles.create({
            id: 51856485165,
            title: "Benchmark des CPU",
            description: "Est-ce qu'AMD a fini par rattraper son retard sur Intel ?",
            createdAt: new Date(),
            updatedAt: new Date(),
            content: null,
            categories: {
                id: 1655626848,
                title: "Informatique",
                slug: "informatique",
                color: "#0f70ab",
                createdAt: new Date(),
                updatedAt: new Date()
            }
        }, {
            include: {
                model: db.categories,
                as: "categories"
            }
        });
        db.articles.create({
            id: 7841526545,
            title: "Qu'est-ce qui est plus fort qu'un lion ?",
            description: "Voici mon top 10 des blagues les plus drôles !",
            createdAt: new Date(),
            updatedAt: new Date(),
            content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultricies erat eget massa sodales molestie. Donec fermentum est quis fringilla ullamcorper. Etiam tempor, nulla non viverra rutrum, turpis elit suscipit felis, ac eleifend nunc turpis sit amet turpis. Phasellus nibh orci, sagittis nec libero sit amet, tristique tempor dui. Integer bibendum ultricies quam, nec vulputate eros tincidunt porttitor. Morbi tincidunt a quam non placerat. Nulla facilisi. In id interdum turpis. In hac habitasse platea dictumst. Pellentesque sit amet odio molestie, hendrerit nunc nec, posuere elit. Aenean vitae pellentesque nulla. Vestibulum sit amet egestas felis.\n" +
                "\n" +
                "Fusce euismod luctus arcu vel luctus. Phasellus eget massa urna. Integer non mauris suscipit, auctor tortor ac, consequat nunc. Sed ut tincidunt tellus. Integer nec turpis vitae ligula mollis blandit. Mauris blandit eu tellus eget tempor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam placerat mattis pharetra. Nulla lobortis justo et augue ornare, vitae vestibulum lacus varius. Etiam in lorem vestibulum, finibus risus et, interdum sem. Nullam pulvinar urna quis velit tristique tristique. Suspendisse condimentum vel eros non volutpat. Nulla porttitor vel velit a efficitur. Sed iaculis odio augue, non efficitur lacus tincidunt ut. Maecenas tincidunt velit ut mattis auctor.\n" +
                "\n" +
                "Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ut augue sed turpis fermentum suscipit eu eu ex. Fusce sagittis sodales eros, at vestibulum nulla semper eget. Phasellus sollicitudin tortor leo, et sollicitudin ex volutpat in. Etiam sed lacus nulla. Fusce quis leo enim. Integer eu quam eros. Morbi pretium rhoncus bibendum. Ut fringilla in orci et ornare. Donec eu lacus commodo, viverra sem sit amet, semper augue. Sed quis molestie odio. Morbi et aliquet ipsum, quis aliquam augue. Cras rutrum ipsum in eros accumsan, nec fermentum diam pretium. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;",
            categories: {
                id: 164851256,
                title: "Humour",
                slug: "humour",
                color: "#E251AE",
                createdAt: new Date(),
                updatedAt: new Date()
            }
        }, {
            include: {
                model: db.categories,
                as: "categories"
            }
        });
    })
    .catch(err => {
        console.log(err);
    })
;
require("./app/routes/category.routes")(app);
require("./app/routes/article.routes")(app);
// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
